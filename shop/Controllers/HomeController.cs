﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using shop.Models;
using Rotativa;

namespace shop.Controllers
{
    public class HomeController : Controller
    {
        MyContext db = new MyContext();

        public ActionResult Index()
        {   
            var categories = db.Categories.ToList();
            return View(categories);
        }

        public ActionResult SelectCategory(string category)
        {
            var selectedCategory = db.Categories.Include("Books").Single(c => c.CategoryName == category);
            return View(selectedCategory);
        }

        public ActionResult SelectCategoryForPdf(string category)
        {
            var selectedCategory = db.Categories.Include("Books").Single(c => c.CategoryName == category);
            return View(selectedCategory);
        }

        public ActionResult PDF(string category)
        {

            return new Rotativa.ActionAsPdf("SelectCategoryForPdf", new { category = category });
        }


        public ActionResult Search(string searchBy, string search)
        {
            if (searchBy == "Author")
            {
                return View(db.Books.Where(a => a.Author.Contains(search) || search == null).ToList());
            }
            if (searchBy == "Title")
            {
                return View(db.Books.Where(a => a.Title.Contains(search) || search == null).ToList());
            }
            if (searchBy == "Description")
            {
                return View(db.Books.Where(a => a.Description.Contains(search) || search == null).ToList());
            }
            if (searchBy == "Image")
            {
                return View(db.Books.Where(a => a.ItemArtUrl != null || search == null).ToList());
            }
            if (searchBy == "Price>")
            {
                decimal searchConvert = Convert.ToDecimal(search);
                return View(db.Books.Where(a => a.Price >= searchConvert || search == null).ToList());
            }
            if (searchBy == "Price<")
            {
                decimal searchConvert = Convert.ToDecimal(search);
                return View(db.Books.Where(a => a.Price <= searchConvert || search == null).ToList());
            }
            return View();
        }

        public ActionResult Details (int id)
        {
            var Book = db.Books.Find(id);
            return View(Book);
        }
        [Authorize(Roles = "admin")]
        public ActionResult Edit()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}