﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using shop.Models;
using System.Net;

namespace shop.Controllers
{
    public class UsersManagerController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: UsersManager
        public ActionResult Index()
        {
            var users = db.Users;
            return View(users.ToList());
        }

        public ActionResult Details(string id)
        {
            IdentityManager im = new IdentityManager();
            ApplicationUser user = im.GetUserByID(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        public ActionResult AddUserToRole(string id)
        {
            IdentityManager im = new IdentityManager();
            ApplicationUser user = im.GetUserByID(id);
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddUserToRole(string id, string role)
        {
            IdentityManager im = new IdentityManager();
            if(im.RoleExists(role))
            {
                im.AddUserToRole(id, role);
                return RedirectToAction("Index");
            }
            else
            {
                im.CreateRole(role);
                im.AddUserToRole(id, role);
                return RedirectToAction("Index");
            }
            return View();
        }
    }
}