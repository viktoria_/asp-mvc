﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace shop.Models
{
    public class MyContext : DbContext
    {
        
        public MyContext() : base("DefaultConnection") { }

        public System.Data.Entity.DbSet<shop.Models.Book> Books { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Stan> Stans { get; set; }
    }
}