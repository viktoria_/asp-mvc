﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace shop.Models
{
    [Bind(Exclude = "ID")]
    public class Book
    {
        [ScaffoldColumn(false)]
        public int ID { get; set; }
        [DisplayName("Category")]
        public int CategoryID { get; set; }
        [Required(ErrorMessage = "A book title is required")]
        public string Title { get; set; }
        public string Author { get; set; }
        [Required(ErrorMessage = "A book price is required")]
        [Range(0.1, 100, ErrorMessage = " Price should be between 0.1 and 100")]
        public decimal Price { get; set; }
        public string Description { get; set; }
        [DisplayName("Book Art Url")]
        public string ItemArtUrl { get; set; }
        [DisplayName("Book Art Url (1)")]
        public string ItemArtUrl_1 { get; set; }
        [DisplayName("Book Art Url (2)")]
        public string ItemArtUrl_2 { get; set; }
        public virtual Category Category { get; set; }

    }
}