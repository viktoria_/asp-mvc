﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace shop.Models
{
    public class Cart
    {
        [Key]
        public int RecordID { get; set;}
        public string CartID { get; set; }
        public int BookID { get; set; }
        public int Count { get; set; }
        public DateTime DateCreated { get; set; }
        public virtual Book Book { get; set; }
    }
}