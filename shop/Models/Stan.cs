﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace shop.Models
{

    public class Stan
    {
        [Key]
        public int StatusID { get; set; }
        public String StatusName { get; set; }
        public List<Order> Orders { get; set; }
    }
}