﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace shop.Models
{
    public class ShoppingCart
    {
        MyContext db = new MyContext();
        string ShoppingCartID { get; set; }
        public const string CartSessionKey = "CartID";
        public static ShoppingCart GetCart(HttpContextBase context)
        {
            var cart = new ShoppingCart();
            cart.ShoppingCartID = cart.GetCartID(context);
            return cart;
        }

        //Helper method to simplify shopping cart calls
        public static ShoppingCart GetCart(Controller controller)
        {
            return GetCart(controller.HttpContext);
        }

        public void AddToCart(Book book)
        {
            var cartBook = db.Carts.SingleOrDefault(
                c => c.CartID == ShoppingCartID
                && c.BookID == book.ID);

            if(cartBook == null)
            {
                cartBook = new Cart
                {
                    BookID = book.ID,
                    CartID = ShoppingCartID,
                    Count = 1,
                    DateCreated = DateTime.Now
                };
                db.Carts.Add(cartBook);
            }
            else
            {
                cartBook.Count++;
            }

            db.SaveChanges();
        }

        public int RemoveFromCart (int id)
        {
            var cartBook = db.Carts.Single(
                cart => cart.CartID == ShoppingCartID
                && cart.RecordID == id);
            int bookCount = 0;

            if(cartBook != null)
            {
                if (cartBook.Count > 1)
                {
                    cartBook.Count--;
                    bookCount = cartBook.Count;
                }
                else
                {
                    db.Carts.Remove(cartBook);
                }
                db.SaveChanges();
            }
            return bookCount;
        }

        public void EmptyCart()
        {
            var cartBooks = db.Carts.Where(
                cart => cart.CartID == ShoppingCartID);
            foreach (var cartBook in cartBooks)
            {
                db.Carts.Remove(cartBook);
            }
            db.SaveChanges();
        }

        public List<Cart> GetCartBooks()
        {
            return db.Carts.Where(
                cart => cart.CartID == ShoppingCartID).ToList();

        }

        public int GetCount()
        {
            int? count = (from cartBooks in db.Carts
                          where cartBooks.CartID == ShoppingCartID
                          select (int?)cartBooks.Count).Sum();
            return count ?? 0;
        }

        public decimal GetTotal()
        {

            decimal? total = (from cartBooks in db.Carts
                              where cartBooks.CartID == ShoppingCartID
                              select (int?)cartBooks.Count *
                              cartBooks.Book.Price).Sum();

            return total ?? decimal.Zero;
        }

        public int CreateOrder(Order order)
        {
            decimal orderTotal = 0;

            var cartBooks = GetCartBooks();

            foreach (var book in cartBooks)
            {
                var orderDetail = new OrderDetail
                {
                    BookID = book.BookID,
                    OrderID = order.OrderID,
                    UnitPrice = book.Book.Price,
                    Quantity = book.Count
                };

                orderTotal += (book.Count * book.Book.Price);

                db.OrderDetails.Add(orderDetail);

            }

            order.Total = orderTotal;


            db.SaveChanges();

            EmptyCart();

            return order.OrderID;
        }

        public string GetCartID(HttpContextBase context)
        {
            if (context.Session[CartSessionKey] == null)
            {
                if (!string.IsNullOrWhiteSpace(context.User.Identity.Name))
                {
                    context.Session[CartSessionKey] =
                        context.User.Identity.Name;
                }
                else
                {

                    Guid tempCartId = Guid.NewGuid();

                    context.Session[CartSessionKey] = tempCartId.ToString();
                }
            }
            return context.Session[CartSessionKey].ToString();
        }

        public void MigrateCart(string Email)
        {
            var shoppingCart = db.Carts.Where(
                c => c.CartID == ShoppingCartID);

            foreach (Cart item in shoppingCart)
            {
                item.CartID = Email;
            }
            db.SaveChanges();
        }

    }
}