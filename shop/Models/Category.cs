﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;


namespace shop.Models
{
    public class Category
    {
        public int CategoryID { get; set; }
        [Required(ErrorMessage = "A category title is required")]
        public string CategoryName { get; set; }
        public List<Book> Books { get; set; }
    }
}