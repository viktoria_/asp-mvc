﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace shop.ViewModels
{
    public class ShoppingCartRemoveViewModel
    {
        public string Message { get; set; }
        public decimal CartTotal { get; set; }
        public int CartCount { get; set; }
        public int BookCount { get; set; }
        public int DeleteID { get; set; }
    }
}