﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using shop.Models;
using System.ComponentModel.DataAnnotations;

namespace shop.ViewModels
{
    public class ShoppingCartViewModel
    {
        
        public List<Cart> CartBooks { get; set; }
        public decimal CartTotal { get; set; }

    }
}